# **Testing Demoblaze.com**

This is a Python project that demonstrates the automated testing of an online store using Selenium and the unittest framework. The tests include checking the title of the webpage, logging in with correct and incorrect credentials, adding and removing items from the cart, and buying an item.

## **Installation**
To use this project, you need to have Python 3 and Selenium installed. 

You also need to have a Selenium Grid setup on your machine. To set up the grid, you can follow the instructions given here: https://www.selenium.dev/documentation/en/grid/. You need to have the hub and at least one node running on your machine.

## **Tests**
The tests included in this project are:

`test_title`

This test checks the title of the webpage and verifies that it contains the word "STORE".

`test_correct_login`

This test logs in to the online store using correct credentials and verifies that the welcome message contains the correct username.

`test_incorrect_login`

This test logs in to the online store using incorrect credentials and verifies that an alert is displayed with the text "Wrong password.".

`test_add_item_to_cart`

This test adds an item to the cart and verifies that a confirmation message is displayed and the item appears in the cart.

`test_remove_item_from_cart`

This test adds an item to the cart, removes it from the cart, and verifies that the item is no longer in the cart.

`test_buying_item`

This test adds an item to the cart, goes to the cart, clicks the "Place Order" button, fills in the form, and verifies that a confirmation message is displayed.

## **Conclusion**
This project demonstrates how to use Selenium and the unittest framework to automate the testing of an online store. By running these tests, you can ensure that the store is functioning correctly and that new changes to the codebase do not introduce any regressions.
