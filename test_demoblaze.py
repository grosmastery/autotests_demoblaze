import unittest
import time
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestingProductStore(unittest.TestCase):

    def setUp(self) -> None:

        hub_url = "http://localhost:4444/wd/hub"
        desired_cap = DesiredCapabilities.CHROME

        self.driver = webdriver.Remote(
            command_executor=hub_url,
            desired_capabilities=desired_cap
            )

        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.get("https://www.demoblaze.com/index.html")


    def test_title(self):

        driver = self.driver
        self.assertIn("STORE", driver.title)


    def test_correct_login(self):

        driver = self.driver

        login_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'login2')))
        login_button.click()

        login_username = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'loginusername')))
        login_username.clear()
        login_username.send_keys("test")

        login_password = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'loginpassword')))
        login_password.clear()
        login_password.send_keys("test")

        confirm_login_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[3]/div/div/div[3]/button[2]')))
        confirm_login_button.click()

        welcome_text = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'nameofuser')))
        self.assertIn("Welcome test", welcome_text.text)


    def test_incorrect_login(self):

        driver = self.driver

        login_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'login2')))
        login_button.click()

        login_username = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'loginusername')))
        login_username.clear()
        login_username.send_keys('test')

        login_password = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'loginpassword')))
        login_password.clear()
        login_password.send_keys('test1')

        confirm_login_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[3]/div/div/div[3]/button[2]')))
        confirm_login_button.click()

        alert = WebDriverWait(driver, 10).until(EC.alert_is_present())
        self.assertIn('Wrong password.', alert.text)


    def test_add_item_to_cart(self):

        driver = self.driver
        
        product = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="tbodyid"]/div[1]/div/a/img')))
        product.click()
        
        add_to_cart = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="tbodyid"]/div[2]/div/a')))
        add_to_cart.click()

        alert = WebDriverWait(driver, 10).until(EC.alert_is_present())
        self.assertIn("Product added", alert.text)
        alert.accept()
        
        cart = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'cartur')))
        cart.click()

        product_in_cart = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="tbodyid"]/tr/td[2]')))
        self.assertIsNotNone(product_in_cart)


    def test_remove_item_from_cart(self):

        driver = self.driver
        
        product = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="tbodyid"]/div[1]/div/a/img')))
        product.click()
        
        add_to_cart = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="tbodyid"]/div[2]/div/a')))
        add_to_cart.click()
        
        alert = WebDriverWait(driver, 10).until(EC.alert_is_present())
        self.assertIn("Product added", alert.text)
        alert.accept()
        
        cart = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'cartur')))
        cart.click()

        delete_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="tbodyid"]/tr/td[4]/a')))
        delete_button.click()
        time.sleep(2) 

        try:
            product_in_cart = driver.find_element(By.XPATH, '//*[@id="tbodyid"]/tr/td[2]')
        except selenium.common.exceptions.NoSuchElementException:
            product_in_cart = None
        self.assertIsNone(product_in_cart)

    
    def test_buying_item(self):

        driver = self.driver

        choose_product_ele = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="tbodyid"]/div[1]/div/a/img')))
        choose_product_ele.click()

        add_to_cart_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="tbodyid"]/div[2]/div/a')))
        add_to_cart_button.click()
        
        alert = WebDriverWait(driver, 10).until(EC.alert_is_present())
        self.assertIn("Product added", alert.text)
        alert.accept()

        cart = driver.find_element(By.ID, 'cartur')
        cart.click()

        place_order_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="page-wrapper"]/div/div[2]/button')))
        place_order_button.click()

        name_ele = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'name')))
        name_ele.send_keys('testName')

        country_ele = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'country')))
        country_ele.send_keys('testCountry')

        city_ele = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'city')))
        city_ele.send_keys('testCity')

        card_ele = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'card')))
        card_ele.send_keys(44415612534)

        month_ele = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'month')))
        month_ele.send_keys(1)    

        year_ele = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'year')))
        year_ele.send_keys(2023) 

        purchase_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="orderModal"]/div/div/div[3]/button[2]')))
        purchase_button.click()

        success_alert = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[10]/h2')))

        self.assertIn('Thank you for your purchase!', success_alert.text)
        

    def products_list(self, driver):
        time.sleep(2)
        element = driver.find_element(By.ID, 'tbodyid')
        return element.text.split("\n")[::3]

    

    def test_categories_buttons(self):

        expecter_result_of_categories = 9
        expecter_result_of_phones = 7
        expecter_result_of_laptops = 6
        expecter_result_of_monitors = 2

        driver = self.driver

        categories_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[5]/div/div[1]/div/a[1]')))
        categories_button.click()

        list_of_categories = self.products_list(driver)
        self.assertEqual(len(list_of_categories), expecter_result_of_categories)

        phones_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[5]/div/div[1]/div/a[2]')))
        phones_button.click()

        list_of_phones = self.products_list(driver)
        self.assertEqual(len(list_of_phones), expecter_result_of_phones)

        laptops_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[5]/div/div[1]/div/a[3]')))
        laptops_button.click()

        list_of_laptops = self.products_list(driver)
        self.assertEqual(len(list_of_laptops), expecter_result_of_laptops)

        monitors_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[5]/div/div[1]/div/a[4]')))
        monitors_button.click()
        
        list_of_monitors = self.products_list(driver)
        self.assertEqual(len(list_of_monitors), expecter_result_of_monitors)


    def tearDown(self) -> None:
        self.driver.quit()

        
if __name__ == '__main__':
    unittest.main()
